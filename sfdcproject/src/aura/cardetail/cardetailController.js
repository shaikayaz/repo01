({
	oncarselected : function(component, event, helper) {
        component.set("v.tabid", event.getParam("car").id);
        component.find("services").reloadRecord();
	},
    
    onrecordupdated : function(component, event, helper) {
        if(component.find("carexp")){
            component.find("carexp").refresh();
        }
    }
})