({
	doInit : function(component, event, helper) {
		helper.onsearch(component, helper);
	},
    
     dosearch : function(component, event, helper){
        var params = event.getParam('arguments');
        
        if(params){
            component.set("v.cartypeidcomponent", params.cartypeid);
            helper.onsearch(component, helper);
        }
    },
    
    
    
    
})