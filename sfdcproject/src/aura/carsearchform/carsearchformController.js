({	
        
    doinit  : function(component, event, helper) {
        var createcarrecord= $A.get("e.force:createRecord()");
        if(createcarrecord){
            component.set("v.shownew", true);
        } else{
             component.set("v.shownew", false);
        }
        
		helper.getcartype(component, helper);
	},
    
	searchclick : function(component, event, helper) {
		//helper.helpersearchclick(component, event, helper);
		var searchformsubmit = component.getEvent("searchformsubmit");
        searchformsubmit.setParams({
            "cartypeid" : component.find("cartypeselected").get("v.value")
        });
        searchformsubmit.fire();
	},
    
   /* toggleclick : function(component, event, helper) {
		var currentvalue=component.get("v.isavail");
            if(currentvalue){
                component.set("v.isavail",false);
            } else{
                 component.set("v.isavail",true);
            }
	},*/
    
    selectedcar  : function(component, event, helper) {
		var currentcar=component.find("cartypeselected").get("v.value");
       alert(currentcar +' option selected');
	},
    
    createrecord : function(component, event, helper) {
        var createcarrecord= $A.get("e.force:createRecord");
        createcarrecord.setParams({
            "entityApiName": "Car_Type__c"
        });
        createcarrecord.fire();
    },

})