({
	oncarclick : function(component, event, helper) {
		var car = component.set("v.car");
        var evt = component.getEvent(oncarselect);
        evt.setParams({
            "carid" : car.id
        });
        evt.fire();
        
        var appevt = $A.get("e.c:carselectedapplicationevent");
        if(appevt){
            appevt.setParams({
                "car" : car
            });
         	appevt.fire();   
        }
	},
    
    
})