({
	create : function(component, event, helper) {
		 var candidate = component.get("v.candidate");
        var action = component.get("c.createrecord");
         
        
        action.setParams({
            candidate : candidate
        });
        
        action.setCallback(this, function(response){

            component.set("v.mapMarkers", [
               		 {
                        location: {
                            Street: 'candidate.Street__c',
                            City: 'candidateCity__c',
                            State: 'candidate.State__c'
                        },
    
                        title: 'The White House',
                        description: 'Landmark, historic home & office of the United States president, with tours for visitors.'
                	}
      			  ]);
      			  component.set("v.zoomLevel", 16); 
            var status = response.getStatus;
            if(status == "SUCCESS"){
                	             $A.get('e.force:refreshView').fire();

                 var newCandidate = {'sobjectType': 'Candidate__c',
                                    'First_Name__c': '',
                                    'Last_Name__c': '',
                                    'Email__c': '', 
                                    'SSN__c': '',
                                    'Street__c': '',
                        			'City__c': '',
                         			'State__c': ''
                                   };
                component.set("v.candidate", newCandidate);
                alert('record created successfully');
                 /*this.showToast({
                        "title": "SUCCESS",
                        "type": "success",
                    	"message": "record is created"
                });*/
            } else if(status == "ERROR"){
                 /*this.showToast({
                        "title": "ERROR",
                        "type": "error",
                    	"message": "record is not created"
                }); */
                alert('record not created');
            }
        });
      
        $A.enqueueAction(action);
        
	},
    
   /* showToast : function(params) {
        var toastEvent = $A.get("e.force:showToast");
        if(toastEvent){
            if(!params){
                toastEvent.setParams({
                    "title": "TOAST ERROR!",
                    "type": "error",
                    "message": "Toast Param not defined"
                });
                toastEvent.fire();
            } else{
                toastEvent.setParams(params);
                toastEvent.fire();
            }
        }
    },  */
    
    
     init: function (cmp, event, helper) {
         
        cmp.set('v.mapMarkers', [
            {
                location: {
                    Street: 'RTC cOLONY',
                    City: 'CHIRALA',
                    State: 'ANDHRA PRADESH'
                },

                title: 'The White House',
                description: 'Landmark, historic home & office of the United States president, with tours for visitors.'
            }
        ]);
        cmp.set('v.zoomLevel', 16);
    },
    
    
})