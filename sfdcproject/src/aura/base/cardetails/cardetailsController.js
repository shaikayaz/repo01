({
	doinit : function(component, event, helper) {
        var navevt = $A.get("e.force:navigateToSObject");
        if(navevt){
            component.set("v.showcardaction", true);
           
        } else{
             component.set("v.showcardaction", false);
        }
        
	},
    
    onfulldetails :  function(component, event, helper) {
        var navevt = $A.get("e.force:navigateToSObject");
        if(navevt){
            navevt.setParams({
                "recordid" : component.get("v.car").Id,
                "slidedevname": "detail"
            });
            navevt.fire();
        } else{
            helper.showtoast(component, {
                "title" : "error",
                "type" : "error",
                "message" : "Event not supported"
            });
        }
    },
})