public with sharing class carsearchcontroller {
    @AuraEnabled
    public static List<Car__c> getcars(String cartypeid){
        if(cartypeid != null && cartypeid.equalsIgnoreCase('')){
            return [select Id, Name, Picture__c, Contact__r.Name, 
                    Geolocation__Latitude__s, Geolocation__Longitude__s 
                    from Car__c 
                    where Available_For_Rent__c = true];
        }
        else{
             return [select Id, Name, Picture__c, Contact__r.Name,
                     Geolocation__Latitude__s, Geolocation__Longitude__s 
                     from Car__c 
                     where Car_Type__c = :cartypeid 
                     and Available_For_Rent__c = true];
        }
    }
    
    

}