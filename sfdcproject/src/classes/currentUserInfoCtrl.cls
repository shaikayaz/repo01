public class currentUserInfoCtrl {
	 @AuraEnabled 
    public static user fetchUser(){ 
      User oUser = [select id, Name, TimeZoneSidKey, Username, Alias, Country, Email, FirstName, LastName, IsActive 
                   FROM User Where id =: userInfo.getUserId()];
        return oUser;
    }
}