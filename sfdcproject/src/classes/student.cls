global class student
{
    public String name{get;set;}
    public Integer age{get;set;}
    
    public student(String name, Integer age){
        this.name=name;
        this.age=age;
    }

}