public class dependentpicklist {
    public String mycity{get;set;}
    public Map<String,List<String>> mybranches=new Map<String,List<String>>();
    public List<selectoption> city{get;set;}
    public List<selectoption> branch{get;set;}
    public pagereference show(){
        List<String> branches=mybranches.get(mycity);
        branch.clear();
        for(String s:branches){
            selectoption op1=new selectoption(s,s);
            branch.add(op1);
        }
        return null;
    }
    
    public dependentpicklist(){
        city=new List<selectoption>();
        branch=new List<selectoption>();
        List<String> hyd=new List<String>();
        hyd.add('srnagar');
        hyd.add('ameerpet');
        hyd.add('panjagutta');
        
        List<String> bang=new List<String>();
        bang.add('ecity');
        bang.add('martali');
        
        mybranches.put('hyd', hyd);
        mybranches.put('bang', bang);
        set<String> keys=mybranches.keySet();
        city.add(new selectoption('null','none'));
        for(String s1:keys){
            selectoption op2=new selectoption(s1,s1);
            city.add(op2);
        }
        
    }
}