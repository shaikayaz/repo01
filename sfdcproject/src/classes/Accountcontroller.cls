public with sharing class Accountcontroller {
	@Auraenabled
    public Static List<Account> findall(){
        return [SELECT id, name, Location__Latitude__s, Location__Longitude__s
            FROM Account
            WHERE Location__Latitude__s != NULL AND Location__Longitude__s != NULL
            LIMIT 50];
    }
}