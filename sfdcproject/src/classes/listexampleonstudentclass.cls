public class listexampleonstudentclass
{
    public list<student> stu{get;set;}
    public List<selectoption> stusel{get;set;}
    public string selectedname{get;set;}
    public listexampleonstudentclass(){
        student s1=new student('ayaz', 26);
        student s2=new student('srikanya',25);
        student s3=new student('sharmila',25);
        stu=new list<student>();
        stu.add(s1);
        stu.add(s2);
        stu.add(s3);
        stusel =new List<selectoption>();
        selectoption se1=new selectoption('this is me','ayaz');
        selectoption se2=new selectoption('my lover','srikanya');
        selectoption se3=new selectoption('my crush','sharmila');
        stusel.add(se1);
        stusel.add(se2);
        stusel.add(se3);
    }
    
}