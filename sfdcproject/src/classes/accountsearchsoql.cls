public class accountsearchsoql {
    public String aname{get;set;}
    public String aaccountnumber{get;set;}
    public String ayearstarted{get;set;}
    public Boolean showresult{get;set;}
    public String msg{get;set;}
    public List<Account> acclist{get;set;}
    
    public accountsearchsoql(){
        showresult=false;
        acclist= new List<Account>();
    }
    
    public void searchaccountdata(){
        if(!String.isBlank(aname) && aname!=Null){
            if(aname.isNumeric()){
                apexpages.Message mymsg=new apexpages.Message(apexpages.Severity.WARNING,'please enter valid name');
                apexpages.addMessage(mymsg);
            }
        }
        
        String aname1,aaccountnumber1,ayearstarted1;
        if(!String.isBlank(aname)|| !String.isBlank(aaccountnumber)|| !String.isBlank(ayearstarted)){
            
            String searchstring='select id,Name,AccountNumber,YearStarted from Account where Name!=Null';
            if(aname !='' && aname !=Null){
                aname1='%'+aname+'%';
                searchstring+='AND Name Like:aname1';
            }
            if(aaccountnumber !='' && aaccountnumber !=Null){
                aaccountnumber1='%'+aaccountnumber+'%';
                searchstring+='AND AccountNumber Like:aaccountnumber1';
            }
             if(ayearstarted !='' && ayearstarted !=Null){
                ayearstarted1='%'+ayearstarted+'%';
                searchstring+='AND YearStarted	 Like:ayearstarted1';
            }
         
            Try{
                System.debug('Final querystring'+searchstring);
                acclist=database.query(searchstring);
                System.debug('Final querystring size'+acclist.size());
                if(acclist.size()>0){
                    showresult=true;
                } 
                else{
                   msg='no records found';
                }
            } 
            catch(Exception e){
                system.debug('exception' +e);
            }
        } else{
            apexpages.Message info=new apexpages.Message(apexpages.Severity.INFO,'please provide atleast account name to perform search');
                apexpages.addMessage(info);

        }    
    }
    
    public pagereference cancel(){
         Pagereference pref = new Pagereference ('/001/o'); 
        pref.setRedirect(true);
        return pref; 
    }
}