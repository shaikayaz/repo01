public class workcentre_controller {

    public List<workcenter__c> NameList { get; set; }

   public workcentre_controller(){
       NameList =new List<workcenter__c>();
   } 

    
    Public string selectedname{get;set;}
     Public List<Selectoption> getwcnames(){
            List<Selectoption> names = new List<selectoption>();
            names.add(new selectOption('', '- None -'));
            for(workcenter__c wcc :[SELECT id,Name from workcenter__c]){
            names.add(new selectoption(wcc.id,wcc.name));
            }
            return names; 
            
        }
        
   public pagereference method1() {
     NameList= [Select Id,Name from workcenter__c where Name=:selectedname];
     return null;
    }
       
    
}