public class YoutubeExample {
    public String results{get;set;}
    public String search{get;set;}
    public void youtubesearch(){
        HttpRequest request=new HttpRequest();
        String url='https://console.developers.google.com/youtube/v3/search?part=snippet&q='+search+'&key=AIzaSyC91SCTX_77tPZ4VjQGASc7JB84PTjIMd4';
        request.setEndpoint(url);
        request.setMethod('GET');
        request.setHeader('Content-type', 'application/json');
        Http p=new Http();
   		HttpResponse response=p.send(request);
        results=response.getBody();
    }
}