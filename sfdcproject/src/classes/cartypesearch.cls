public with sharing class cartypesearch {
	@AuraEnabled
    public Static List<Car_Type__c> getcartypes(){
        return [select Id, Name from Car_Type__c];
    }
}