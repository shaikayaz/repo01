global class batchapexforaccount implements database.batchable<sObject> {
    global database.QueryLocator start(database.BatchableContext bc){
        return database.getQueryLocator('select id,name from Account');
    }
    global void execute(database.BatchableContext bc, List<sObject> scope){
        list<Account> acc=new list<account>();
        for(sobject x:scope){
            account a=(account)x;
            a.Name='mb.'+a.Name;
            acc.add(a);
        }
        update acc;
    }
    global void finish(database.BatchableContext bc){
        
    }
}