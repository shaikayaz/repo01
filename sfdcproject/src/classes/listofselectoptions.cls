public class listofselectoptions {
    public String selectedvalue{get;set;}
    public List<selectoption> myoptions;
    public List<selectoption> getmyoptions(){
        return myoptions;
    }
    public listofselectoptions(){
        myoptions=new List<selectoption>();
        selectoption op1=new selectoption('-none-','-null-');
        selectoption op2=new selectoption('one','January');
        selectoption op3=new selectoption('two','february');
        myoptions.add(op1);
        myoptions.add(op2);
        myoptions.add(op3);
        myoptions.add(new selectoption('three','march'));
        myoptions.add(new selectoption('four','april'));
    }
}