global class batchapexoncustomer implements database.Batchable<sObject>{
	public String myname;
    public batchapexoncustomer(String myname){
        this.myname=myname;
        
    }
    global database.QueryLocator start(database.BatchableContext bc){
        String query='select id,name,AccountType__c from Customer__c';
        return database.getQueryLocator(query);
    }
    global void execute(database.BatchableContext bc,List<Customer__c> scope){
        List<Customer__c> cus=new List<Customer__c>();
        for(Customer__c c:Cus){
            c.AccountType__c='savings';
            cus.add(c);
        }
        update cus;
    }
    global void finish(database.BatchableContext bc){}
}