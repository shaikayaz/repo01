public class SchemaExample {
    public String selectedobject{get;set;} 
	public List<SelectOption> options;
    public List<SelectOption> childopt;
    public List<SelectOption> pickopt;
     public List<SelectOption> fieldopt;
    public List<SelectOption> getoptions(){
        return options;
    }
     public List<SelectOption> getchildopt(){
        return childopt;
    }
     public List<SelectOption> getpickopt(){
        return pickopt;
    }
    public List<SelectOption> getfieldopt(){
        return fieldopt;
    }
    
    public SchemaExample(){
        options=new List<SelectOption>();
        Map<String,Schema.SObjectType> m= Schema.getGlobalDescribe();
        set<String> objs=m.keyset();
        for(String s:objs){
             SelectOption op=new SelectOption(s,s);
        options.add(op);
        } 
        childopt=new List<SelectOption>();
        Schema.DescribeSObjectResult r= Account.SObjectType.getDescribe();
        List<Schema.ChildRelationship> c=r.getChildRelationships();
        for(schema.ChildRelationship cr:c){
            String name=''+cr.getChildSObject();
            selectoption op=new selectoption(name,name);
            childopt.add(op);
        }
        pickopt=new List<SelectOption>();
        schema.DescribeFieldResult fr=Account.Industry.getDescribe();
        List<schema.PicklistEntry> pi=fr.getpickListValues();
        for(PicklistEntry x:pi){
            selectoption op=new selectoption(x.getLabel(),x.getLabel());
            pickopt.add(op);
        }
        fieldopt=new List<selectoption>();
        Map<String,Schema.SObjectField> fieldmap=schema.SObjectType.Account.Fields.getMap();
        Set<String> fieldset=fieldmap.KeySet();
        for(String s:fieldset){
            selectoption op=new selectoption(s,s);
            fieldopt.add(op);
        }
    }
}