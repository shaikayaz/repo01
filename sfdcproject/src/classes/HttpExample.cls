public class HttpExample {
    public String result{get;set;}
    public String ipaddress{get;set;}
    public String status{get;set;}
    public Integer statuscode{get;set;}
    public Map<String,string> xmlmap{get;set;}
    public void submit(){
        HttpRequest request= new HttpRequest();
        request.setEndpoint('http://xml.utrace.de/?query='+ipaddress);
        request.setMethod('GET');
        Http p=new Http();
        HttpResponse response=p.send(request);
        result=response.getBody();
        statuscode=response.getStatusCode();
        status=response.getStatus();
        xmlmap=new Map<String,string>();
        DOM.Document doc=new DOM.Document();
        doc.load(result);
        DOM.XmlNode root=doc.getRootElement();
        for(DOM.XmlNode child:root.getChildElements()){ 
            for(DOM.XmlNode v:child.getChildElements()){
                xmlmap.put(v.getname(), v.getText());
            }
        }       
    }   
}