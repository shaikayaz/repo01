public class Displayingfieldsonobjectselection {
    public String oname{get;set;}
    Map<String,Schema.SObjectType> my;
    public List<SelectOption> myoptions;
    public List<SelectOption> options;
    public List<selectoption> getmyoptions(){
        return myoptions;
    }
     public List<selectoption> getoptions(){
        return options;
    }
    public Displayingfieldsonobjectselection(){
        my=Schema.getGlobalDescribe();
        myoptions=new List<SelectOption>();
        options=new List<SelectOption>();
        set<String> objset=my.keySet();
        
        for(String s:objset){
            if(my.get(s).getDescribe().isCustom()){
                Selectoption op=new selectoption(s,s);
                myoptions.add(op);
            }
        }   
    }
    
    public pagereference show(){
        Map<String,Schema.SObjectField> m=my.get(oname).getDescribe().Fields.getMap();
        set<String> fieldset=m.keyset();
        for(String s:fieldset){
            selectoption op=new selectoption(s,s);
            options.add(op);
        }
        return null;
    }
}