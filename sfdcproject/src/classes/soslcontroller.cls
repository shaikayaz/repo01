public with sharing class soslcontroller {
public string searchword{get;set;}
public List<Account> acclist{get;set;}
public List<Contact> conlist{get;set;}
public List<Opportunity> opplist{get;set;}

public soslcontroller(){
acclist=new List<Account>();
conlist=new List<Contact>();
opplist=new List<Opportunity>();
}

public void searchmethod(){
acclist=new List<Account>();
conlist=new List<Contact>();
opplist=new List<Opportunity>();

if(searchword.Length()>1){
String searchword1='*'+searchword+'*';
String searchquery='FIND \''+searchword+'\' IN ALL FIELDS RETURNING Account(Name,Type),Contact(Name,Email),Opportunity(Name,StageName) ';
List<List<sObject>> searchlist=search.query(searchquery);
acclist=(List<Account>)searchlist[0];
conlist=(List<Contact>)searchlist[1];
opplist=(List<Opportunity>)searchlist[2];
if(acclist.size()==0 && conlist.size()==0 && opplist.size()==0){
apexPages.addmessage(new apexpages.message(apexpages.severity.Error, 'sorry no results found with the provided word'));
return;
}

} else{
        apexPages.addmessage(new apexpages.message(apexpages.severity.Error, 'please type atleast 2 characters'));
        return;
}
}
}